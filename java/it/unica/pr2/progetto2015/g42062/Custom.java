package it.unica.pr2.progetto2015.g42062;

import java.net.URL;
import it.unica.pr2.progetto2015.interfacce.SheetFunction;
import it.sauronsoftware.feed4j.FeedParser;
import it.sauronsoftware.feed4j.bean.Feed;
import it.sauronsoftware.feed4j.bean.FeedHeader;
import it.sauronsoftware.feed4j.bean.FeedItem;
import it.sauronsoftware.feed4j.FeedIOException;
import it.sauronsoftware.feed4j.FeedXMLParseException;
import it.sauronsoftware.feed4j.UnsupportedFeedException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Funzione Custom - Effettua la ricerca di una stringa nei feed RSS trovati all'indirizzo passato come parametro
 * e restituisce gli elementi che la contengono.
 * Utilizza la libreria esterna it.sauronsoftware.feed4j
 * @author antonello
 */
public class Custom implements SheetFunction {

    /**
     * Contiene l'header e gli elementi recuperati che contengono la stringa di ricerca
     */
    List<String> leggiRSS = new ArrayList<String>();

    /**
     * Esegue la Funzione Custom "LEGGI.RSS"
     * @param args - Array di Object: in posizione 0 la URL del RSS, in posizione 1 la stringa di ricerca
     * @return String[] - Restituisce un array di stringhe contenenti lo header e gli elementi trovati
     */
    @Override
    public Object execute(Object[] args) {
        URL feedUrl = (URL) args[0];
        String ricerca = (String) args[1];
        if (ricerca.equals("0.0")){
            ricerca=" ";
        }
        String titoloFeed; //usata per la ricerca nel titolo e nella descrizione dell'elemento RSS

        Feed feed;
        try {

            feed = FeedParser.parse(feedUrl);
            leggiRSS.add("** HEADER **");

            FeedHeader header = feed.getHeader();
            leggiRSS.add("Titolo: " + header.getTitle()+"\nLink: " + header.getLink()+
                    "\nDescrizione: " + header.getDescription()+"\nLingua: " + header.getLanguage());
            leggiRSS.add(" ");
            
            leggiRSS.add("** ITEMS **");
            int items = feed.getItemCount();
            for (int i = 0; i < items; i++) {
                FeedItem item = feed.getItem(i);
                titoloFeed = item.getTitle()+" "+item.getDescriptionAsText();
                if (titoloFeed.indexOf(ricerca) != -1) {
                leggiRSS.add("Titolo: " + item.getTitle()+"\n"+
                        "Link: " + item.getLink()+"\n"+"Descrizione: " + item.getDescriptionAsText());
                leggiRSS.add(" ");

                }
            }
        /** 
         * Gestione delle eccezioni
         */
        } catch (FeedIOException e) {
            return "Errore IO";
        } catch (FeedXMLParseException e) {
            return "Errore di parse";
        } catch (UnsupportedFeedException e) {
            return "feed non supportato";
        }

        int i = 0;
        /**
         * Prepara la matrice di stringhe per la corretta visualizzazione degli elementi
         * sul foglio di calcolo - la prima colonna contiene solo spazi - Restituendo un
         * array di stringhe la visualizzazione sul foglio di calcolo avveniva su una sola riga
         * con questo espediente invece si è potuto incolonnare il risultato
         */
        String temp;
        String[][] RSS = new String[leggiRSS.size()][2];
        Iterator<String> it = leggiRSS.iterator(); //utilizza un iteratore per ottenere gli elementi della lista
        while (it.hasNext()) {
            temp = it.next();
            RSS[i][0] = " ";
            RSS[i][1] = temp;
            i++;
        }

        return RSS;
    }

    @Override
    public String getCategory() {
        return "Custom";
    }

    @Override
    public String getHelp() {
        return "Restituisce gli RSS presenti nell'URL inserita come parametro che contengono il testo specificato (Case sensitive)";
    }

    @Override
    public String getName() {
        return "LEGGI.RSS";
    }

}
