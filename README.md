# README #
Università di Cagliari
Anno Accademico 2014/2015
Progetto Pr2
Realizzazione delle funzioni Semplice, Complessa e Custom
per foglio di calcolo Libreoffice
Autore: Antonello Meloni
Matricola: 42062

Funzione Semplice:
Calcolo della data della domenica di Pasqua a partire dal numero dell'anno
Algoritmo utilizzato: Oudin modificato da Claus Tondering;

Funzione Complessa:
Calcolo del determinante di una matrice quadrata n*n
Algoritmo utilizzato: Gauss (riduzione della matrice a una matrice triangolare mediante combinazione lineare delle righe e scambi di posizione di righe in caso di elementi nulli in posizioni critiche)

Funzione Custom:
Ricerca una stringa di testo nei feed RSS di un indirizzo fornito e restituisce i feed che la contengono
Libreria esterna usata: it.sauronsoftware.feed4j 