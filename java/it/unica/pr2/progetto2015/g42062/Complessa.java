package it.unica.pr2.progetto2015.g42062;

import it.unica.pr2.progetto2015.interfacce.SheetFunction;

public class Complessa implements SheetFunction {

    Double[][] matrix;
    int scambi = 1;

    /**
     * Calcola il determinante di matrici quadrate n*n con l'algoritmo di GAUSS
     * mediante riduzione a matrice triangolare
     * @param args - Matrice quadrata di Double
     * @return Double - Determinante della matrice quadrata
     */
    @Override
    public Object execute(Object[] args) {
        
        this.matrix = (Double[][]) args[0];

        return gauss();
    }

    @Override
    public String getCategory() {
        return "Matrice";
    }

    @Override
    public String getHelp() {
        return "Calcola il determinante di una matrice quadrata n*n prendendo in input n*n celle del foglio di calcolo";
    }

    @Override
    public String getName() {
        return "MATR.DETERM";
    }

    /**
     * Funzione di calcolo del determinante mediante riduzione della matrice ad
     * una matrice triangolare
     *
     * @return Double valore del determinante
     */
    private Object gauss() {
        int i, j, k;                //variabili intere per i for
        double m;                   // double usato nel calcolo per evitare inutili accessi alla matrice 
        double determinante = 1;      // valore determinante

        int n = matrix[0].length;

        if (n != matrix.length) {   //verifica se la matrice è quadrata
            return "Errore: non è stata immessa una matrice quadrata";
        }

        /**
         * Verifica esistenza righe nulle
         */
        for (i = 0; i < n; i++) {
            k = 0;
            for (j = 0; j < n; j++) {
                if (matrix[i][j] != 0) {
                    k = 1;
                }

            }
            if (k == 0) {
                return 0.0;
            }
        }
        /**
         * Metodo di Gauss con verifica pivot!=0
         */
        for (k = 0; k < n; k++) {
            if (!scambio(k)) {
                return 0.0;
            }
            for (i = k + 1; i < n; i++) {
                m = matrix[i][k] / matrix[k][k];
                for (j = k; j < n; j++) {
                    matrix[i][j] = matrix[i][j] - m * matrix[k][j];
                }
            }
        }
        /**
         * Calcolo determinante mediante moltiplicazione dei valori sulla
         * diagonale principale
         */
        for (i = 0; i < n; i++) {
            determinante *= matrix[i][i];
        }
        /**
         * Restituisce valore determinante corretto con un cambio di segno per
         * ogni scambio di riga effettuato dalla funzione scambio
         */
        return determinante * scambi;
    }

    /**
     * Funzione privata necessaria per evitare che il metodo di riduzione di
     * Gauss divida i coeff della matrice per zero
     *
     * @param pivot
     * @return boolean true se, in caso di pivot nullo, è riuscito almeno uno
     * scambio con un valore!=0
     */
    private boolean scambio(int pivot) {
        int i, j, k;
        Double scambio;
        int n = matrix[0].length;

        /**
         * Se il valore è nullo cerca nelle righe successive della matrice un
         * valore non nullo per effettuare uno scambio di riga
         */
        if (matrix[pivot][pivot] == 0) {
            i = 0;
            for (k = pivot + 1; k < n; k++) {
                if (matrix[k][pivot] != 0) {
                    scambi *= -1;
                    for (j = 0; j < n; j++) {
                        scambio = matrix[k][j];
                        matrix[k][j] = matrix[pivot][j];
                        matrix[pivot][j] = scambio;
                    }
                    i = 1;
                }
            }
            if (i == 0) {
                return false;
            }
        }
        return true;
    }
}
