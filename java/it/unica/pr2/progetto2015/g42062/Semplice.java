package it.unica.pr2.progetto2015.g42062;

import it.unica.pr2.progetto2015.interfacce.SheetFunction;

/**
 * Funzione Semplice Immettendo l'anno restituisce la data del giorno di Pasqua: nell'intervallo (100-1582), 
 * secondo il calendario Giuliano, per l'intervallo 1583-9999 secondo quello Gregoriano
 * Si mantiene l'attuale convenzione, adottata nei fogli di calcolo, che gli anni 30-99 si riferiscano al 
 * periodo 1930-1999 e gli anni 0-29 al periodo 2000-2029
 * @author antonello
 */
public class Semplice implements SheetFunction {
    /**
     * Immettendo l'anno restituisce la data del giorno di Pasqua: nell'intervallo (100-1582), 
     * secondo il calendario Giuliano, per l'intervallo 1583-9999 secondo quello Gregoriano
     * @param args
     * @return String - Restituisce la data della domenica di Pasqua 
     */
    @Override
    public Object execute(Object[] args) {
        int anno = (int) args[0];
        if (anno>9999){
            return"#VALORE!";
        }
        int c, i, j, l, g, h;

        if (anno >= 30 && anno < 99) {
            anno += 1900;
        } else if (anno >= 0 && anno < 30) {
             anno += 2000;
        }
               
        g = anno % 19;
        
        if (anno < 1583) {
            
            i = (19 * g + 15) % 30;
            j = (anno + anno / 4 + i) % 7;
        } else {
            c = anno / 100;
            h = (c - c / 4 - (8 * c + 13) / 25 + 19 * g + 15) % 30;
            i = h - h / 28 * (1 - 29 / (h + 1) * (21 - g) / 11);
            j = (anno + anno / 4 + i + 2 - c + c / 4) % 7;
        }

        l = i - j;
        
        int meseDiPasqua = 3 + (l + 40) / 44;
        int giornoDiPasqua = l + 28 - 31 * (meseDiPasqua / 4);
        String separatore1 = "";
        String separatore2 = "";

        if (giornoDiPasqua < 10) {
            separatore1 = "0";
        }

        if (meseDiPasqua < 10) {
            separatore2 = "0";
        }
        return separatore1 + giornoDiPasqua + "/" + separatore2 + meseDiPasqua + "/" + anno;
    }

    @Override
    public String getCategory() {
        return "Data&Orario";
    }

    @Override
    public String getHelp() {
        return "Immettendo l'anno restituisce la data del giorno di Pasqua: nell'intervallo (100-1582) secondo il calendario Giuliano, nell'intervallo 1583-9999 secondo quello Gregoriano";
    }

    @Override
    public String getName() {
        return "DOMENICA.DI.PASQUA";
    }

}
